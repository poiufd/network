#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/ioctl.h>
#include <sys/sysmacros.h>
#include <fcntl.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>

#include "filter.h"

int main(int argc, char **argv)
{
	int fd;
	unsigned int addr;

	if (argc != 2) {
		printf("wrong argument count\n");
		return 1;
	}

	addr = inet_addr(argv[1]);
	fd = open("/dev/filter", O_RDONLY);

	if (fd < 0) {
		perror("open");
	} else {
		if (ioctl(fd, FILTER_IOCSADDR, &addr) < 0)
			perror("ioctl error");

		if (close(fd) < 0)
			perror("close");
	}

	return 0;
}
