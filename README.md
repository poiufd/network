## test tcp filter
```bash
nc -l -p 60000 &
nc -q 2 127.0.0.1 60000
```
## test tcp server
```bash
netstat -tuan
nc -q 4 127.0.0.1 60000 -p 60001
```
## test udp message receiving
```bash
nc -l -u -p 60001
```
