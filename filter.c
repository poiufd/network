#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/errno.h>
#include <linux/types.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/net.h>
#include <linux/in.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/tcp.h>
#include <linux/inet.h>

#include <linux/cdev.h>

#include "filter.h"

static struct cdev cdev;
static dev_t dev;

static u32 ip_address;
static unsigned int filter_ip_daddr = 1;

module_param(filter_ip_daddr, uint, 0444);

static int filter_open(struct inode *inode, struct file *filp)
{
	return 0;
}

static int filter_release(struct inode *inode, struct file *filp)
{
	return 0;
}

static long filter_ioctl(struct file *filp, unsigned int cmd, unsigned long arg)
{
	int retval = 0;

	if ((_IOC_TYPE(cmd) != FILTER_IOC_MAGIC) ||
		(_IOC_NR(cmd) > FILTER_IOC_MAXNR))
		return -ENOTTY;

	switch (cmd) {
	case FILTER_IOCSADDR:
		retval = get_user(ip_address, (unsigned int __user *)arg);
		break;
	default:
		retval = -ENOTTY;
	}

	return retval;
}

static const struct file_operations filter_fops = {
	.owner		= THIS_MODULE,
	.unlocked_ioctl = filter_ioctl,
	.open		= filter_open,
	.release	= filter_release,
};

static int compare_address(u32 address)
{
	if (filter_ip_daddr)
		return address == ip_address;

	return 1;
}

static unsigned int filter_nf_hookfn(void *priv, struct sk_buff *skb,
	const struct nf_hook_state *state)
{
	struct tcphdr *tcph;
	struct iphdr *iph = ip_hdr(skb);

	if (iph->protocol == IPPROTO_TCP) {
		tcph = tcp_hdr(skb);
		if ((tcph->syn && !tcph->ack) &&
			(compare_address(iph->daddr) > 0))
			pr_info("source ip address: %pI4, source port: %u\n",
				&iph->saddr, ntohs(tcph->source));
	}

	return NF_ACCEPT;
}

static struct nf_hook_ops filter_nf_hook_ops = {
	.hook		= filter_nf_hookfn,
	.hooknum	= NF_INET_LOCAL_OUT,
	.pf		= PF_INET,
	.priority	= NF_IP_PRI_FIRST
};

static int filter_module_init(void)
{
	int retval;

	retval = alloc_chrdev_region(&dev, 0 /* firstminor */, 1 /* count */,
		"filter" /* name */);
	if (retval < 0) {
		pr_info("filter: can't get major number\n");
		return retval;
	}

	cdev_init(&cdev, &filter_fops);
	cdev.owner = THIS_MODULE;

	retval = cdev_add(&cdev, dev, 1 /* count */);
	if (retval < 0) {
		pr_info("filter: error adding device: %d\n", retval);
		goto free_chrdev_region;
	}

	ip_address = in_aton("127.0.0.2");

	retval = nf_register_net_hook(&init_net, &filter_nf_hook_ops);
	if (retval < 0) {
		pr_info("filter: error registering net hook: %d\n", retval);
		goto free_cdev;
	}

	return 0;

free_cdev:
	cdev_del(&cdev);
free_chrdev_region:
	unregister_chrdev_region(dev, 1 /* count */);

	return retval;
}

static void filter_module_exit(void)
{
	nf_unregister_net_hook(&init_net, &filter_nf_hook_ops);
	cdev_del(&cdev);
	unregister_chrdev_region(dev, 1 /* count */);
}

module_init(filter_module_init);
module_exit(filter_module_exit);

MODULE_LICENSE("Dual BSD/GPL");
