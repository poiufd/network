#ifndef _FILTER_H_
#define _FILTER_H_

#include <linux/ioctl.h>

#define FILTER_IOC_MAGIC	0x82
#define FILTER_IOCSADDR		_IOW(FILTER_IOC_MAGIC, 0, unsigned int)
#define FILTER_IOC_MAXNR	1

#endif /* _FILTER_H_ */
