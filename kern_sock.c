#include <linux/module.h>
#include <linux/init.h>
#include <linux/kernel.h>
#include <linux/net.h>
#include <linux/in.h>
#include <net/sock.h>

#define PORT 60000
#define UDP_PORT 60001

static struct socket *sock;
static struct socket *newsock;
static struct socket *udp_sock;

static int send_message(void)
{
	struct msghdr msg;
	struct iovec iov;
	struct sockaddr_in addr = {
		.sin_family	= AF_INET,
		.sin_port	= htons(UDP_PORT),
		.sin_addr	= { htonl(INADDR_LOOPBACK) }
	};
	char *buffer = "test_message";
	size_t len = strlen(buffer) + 1;

	iov.iov_base = buffer;
	iov.iov_len = len;
	msg.msg_flags = 0;
	msg.msg_name = &addr;
	msg.msg_namelen = sizeof(addr);
	msg.msg_control = NULL;
	msg.msg_controllen = 0;

	return kernel_sendmsg(udp_sock, &msg, (struct kvec *) &iov,
		1 /* num */, len);
}

static int kern_sock_module_init(void)
{
	int retval;
	struct sockaddr_in peer_addr, addr = {
		.sin_family	= AF_INET,
		.sin_port	= htons(PORT),
		.sin_addr	= { htonl(INADDR_LOOPBACK) }
	};

	/* UDP task */
	retval = sock_create_kern(&init_net, PF_INET, SOCK_DGRAM, IPPROTO_UDP,
		&udp_sock);
	if (retval < 0) {
		pr_info("error creating udp socket: %d\n", retval);
		return retval;
	}

	retval = send_message();
	if (retval < 0) {
		pr_info("error sending udp message: %d\n", retval);
		goto free_udp_socket;
	}

	/* TCP task */
	retval = sock_create_kern(&init_net, PF_INET, SOCK_STREAM,
		IPPROTO_TCP, &sock);
	if (retval < 0) {
		pr_info("error creating tcp socket: %d\n", retval);
		goto free_udp_socket;
	}

	retval = sock->ops->bind(sock, (struct sockaddr *)&addr, sizeof(addr));
	if (retval < 0) {
		pr_info("error binding socket: %d\n", retval);
		goto free_socket;
	}

	retval = sock->ops->listen(sock, SOMAXCONN);
	if (retval < 0) {
		pr_info("error putting in the listenning state: %d\n", retval);
		goto free_socket;
	}

	retval = sock_create_lite(PF_INET, sock->type, IPPROTO_TCP, &newsock);
	if (retval < 0) {
		pr_info("error creating new socket: %d\n", retval);
		goto free_socket;
	}

	newsock->ops = sock->ops;

	retval = sock->ops->accept(sock, newsock, 0 /* flags */, true /* kern */);
	if (retval < 0) {
		pr_info("error accepting new socket: %d\n", retval);
		goto free_new_socket;
	}

	retval = sock->ops->getname(newsock, (struct sockaddr *)&peer_addr, 1 /* peer */);
	if (retval < 0) {
		pr_info("error getting peer address: %d\n", retval);
		goto free_new_socket;
	}

	pr_info("connection established to %pI4:%d\n",
		&peer_addr.sin_addr.s_addr, ntohs(peer_addr.sin_port));

	return 0;

free_new_socket:
	sock_release(newsock);
free_socket:
	sock_release(sock);
free_udp_socket:
	sock_release(udp_sock);

	return retval;
}

static void kern_sock_module_exit(void)
{
	if (newsock)
		sock_release(newsock);
	if (sock)
		sock_release(sock);
	if (udp_sock)
		sock_release(udp_sock);
}

module_init(kern_sock_module_init);
module_exit(kern_sock_module_exit);

MODULE_LICENSE("Dual BSD/GPL");
